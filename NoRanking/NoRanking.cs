﻿using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using LLBML.Utils;

namespace NoRanking
{
    [BepInPlugin(PluginInfos.PLUGIN_ID, PluginInfos.PLUGIN_NAME, PluginInfos.PLUGIN_VERSION)]
    [BepInDependency("no.mrgentle.plugins.llb.modmenu", BepInDependency.DependencyFlags.SoftDependency)]
    [BepInProcess("LLBlaze.exe")]
    public class NoRanking : BaseUnityPlugin
    {

        internal static ConfigEntry<bool> hideRankInMenu;
        internal static ConfigEntry<bool> hideRankInStageSelect;
        internal static ConfigEntry<bool> hideRankInGameResult;

        // Awake is called once when both the game libs and the plug-in are loaded
        void Awake()
        {
            Logger.LogInfo("Hello, world!");

            hideRankInMenu = Config.Bind("Toggles", "hideRankInMenu", true);
            hideRankInStageSelect = Config.Bind("Toggles", "hideRankInStageSelect", true);
            hideRankInGameResult = Config.Bind("Toggles", "hideRankInGameResult", true);

            var harmoInstance = new Harmony(PluginInfos.PLUGIN_ID);
            Logger.LogDebug("Patching game results");
            harmoInstance.PatchAll(typeof(GameResultPatch));
            Logger.LogDebug("Patching game results sound");
            harmoInstance.PatchAll(typeof(GameResultSoundPatch));
            Logger.LogDebug("Patching main menu");
            harmoInstance.PatchAll(typeof(MainMenuPatch));
            Logger.LogDebug("Patching Stage Select");
            harmoInstance.PatchAll(typeof(StageSelectPatch));
        }

        void Start()
        {
            ModDependenciesUtils.RegisterToModMenu(this.Info);
        }
    }
}
