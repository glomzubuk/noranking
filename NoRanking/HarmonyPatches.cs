﻿using System;
using System.Diagnostics;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using LLHandlers;
using HarmonyLib;

namespace NoRanking
{

    public static class MainMenuPatch
    {
        [HarmonyPatch(typeof(FBKNPOJNCKN), "LDBIMDECNHB")]
        [HarmonyPostfix]
        // Passthrough postfixes take only the return from the original method as regular argument
        // Here i use it to break a Coroutine
        public static IEnumerator RemoveMenuDiv_PassthroughPostfix(IEnumerator result, EMFEJCFPLBP __instance)
        {
            if (NoRanking.hideRankInMenu.Value)
            {
                Traverse profileInfo = Traverse.Create(__instance);
                profileInfo.Field<Image>("GLGEMDAIKEO").Value.enabled = false;
                yield break;
            }
            else
            {
                yield return result;
            }
        }
    }

    public static class StageSelectPatch
    {
        // Same thing, here i used nameof because the method is public, seems cleaner, but it doesn't change anything
        [HarmonyPatch(typeof(ScreenPlayersStageComp), nameof(ScreenPlayersStageComp.OnOpen))]
        // As for prefixes, there can be many per methods
        [HarmonyPostfix]
        // Postfix either have a void return type, as this one does, or the same return type as the target, in which case it is called "passthrough"
        // Regular void return postfixes can use the __result argument as well, so the passthrough syntax is optionnal but handy in some cases (example incoming)
        public static void RemoveStageSelectDiv_Postfix(ScreenPlayersStageComp __instance)
        {

            if (NoRanking.hideRankInStageSelect.Value)
            {
                DisableImDivisionInComponent(__instance.opponentInfo);
                DisableImDivisionInComponent(__instance.userInfo);
            }
        }

        public static void DisableImDivisionInComponent(Component component)
        {
            foreach (Image image in component.GetComponentsInChildren<Image>(false))
            {
                if (image.name == "imDivision")
                {
                    image.enabled = false;
                }
            }
        }
    }

    public static class GameResultPatch
    {
        /*
        [HarmonyPatch(typeof(EMFEJCFPLBP), "DKDLEPKFNFF")]
        [HarmonyPostfix]
        public static IEnumerator RemoveLocalPlayerGameResultEmblem_PassthroughPostfix(IEnumerator incoming, EMFEJCFPLBP __instance)
        {
            Traverse<Image> traverse = Traverse.Create(__instance).Field<Image>("LEAPFKIPPJF");
            traverse.Value.enabled = false;
            yield break;
        }
        */

        [HarmonyPatch(typeof(EMFEJCFPLBP), "PAGAAPAERA")]
        [HarmonyPostfix]
        public static void RemovePlayersGameResultEmblemAndSliders_Postfix(EMFEJCFPLBP __instance)
        {
            if (NoRanking.hideRankInGameResult.Value)
            {
                Traverse ranking = Traverse.Create(__instance);
                ranking.Field<Image>("LEAPFKIPPJF").Value.gameObject.SetActive(false);
                ranking.Field<Image>("LEAPFKIPPJF").Value.enabled = false;
                ranking.Field<Slider>("DFNGIPICJPL").Value.gameObject.SetActive(false);
                ranking.Field<Slider>("DFNGIPICJPL").Value.enabled = false;
                ranking.Field<Slider>("CDENCKCAJJK").Value.gameObject.SetActive(false);
                ranking.Field<Slider>("CDENCKCAJJK").Value.enabled = false;
                ranking.Field<Slider>("CPFGENLCJME").Value.gameObject.SetActive(false);
                ranking.Field<Slider>("CPFGENLCJME").Value.enabled = false;
                ranking.Field<RectTransform>("EEGEAJMGPIF").Value.gameObject.SetActive(false);
                ranking.Field<RectTransform>("EEGEAJMGPIF").Value.position = new Vector3(-2000, -2000, -2000);
            }
        }

        [HarmonyPatch(typeof(EMFEJCFPLBP), "APFPMIEKFAD")]
        [HarmonyPostfix]
        public static IEnumerator RemoveRankChangeAnimation_PassthroughPostfix(IEnumerator incoming, EMFEJCFPLBP __instance)
        {
            if (NoRanking.hideRankInGameResult.Value)
            {
                Traverse ranking = Traverse.Create(__instance);
                ranking.Field<Image>("LEAPFKIPPJF").Value.enabled = false;
                ranking.Field<Slider>("CPFGENLCJME").Value.enabled = false;
                yield break;
            }
            else
            {
                yield return incoming;
            }
        }

    }

    public static class GameResultSoundPatch
    {
        [HarmonyPatch(typeof(EMFEJCFPLBP), "BHHHBJOAPNB")]
        [HarmonyPatch(typeof(EMFEJCFPLBP), "FEGAELBICJN")]
        [HarmonyPatch(typeof(EMFEJCFPLBP), "EGCBMADJBNA")]
        [HarmonyPatch(typeof(EMFEJCFPLBP), "JPMJGCBIMLB")]
        [HarmonyPatch(typeof(EMFEJCFPLBP), "FMAMNBCOOFD")]
        [HarmonyPatch(typeof(EMFEJCFPLBP), "MHOGBLALADB")]
        [HarmonyPatch(typeof(EMFEJCFPLBP), "NGBBNOLKGJL")]
        [HarmonyPatch(typeof(EMFEJCFPLBP), "BPIAHGAKCHP")]
        [HarmonyPatch(typeof(EMFEJCFPLBP), "CKPCIKIOBME")]
        [HarmonyPatch(typeof(EMFEJCFPLBP), "DICEDDEOGOJ")]
        [HarmonyPatch(typeof(EMFEJCFPLBP), "INAAEAGAHPH")]
        [HarmonyPatch(typeof(EMFEJCFPLBP), "APFPMIEKFAD")]
        [HarmonyPatch(typeof(EMFEJCFPLBP), "ANLNOPCNMEJ")]
        [HarmonyPatch(typeof(EMFEJCFPLBP), "DBENBLGDHND")]
        [HarmonyPatch(typeof(EMFEJCFPLBP), "OIHHHAMLMLF")]
        [HarmonyPatch(typeof(EMFEJCFPLBP), "FINAEBFBBFK")]
        [HarmonyPatch(typeof(EMFEJCFPLBP), "FORPAKGANEN")]
        [HarmonyPatch(typeof(EMFEJCFPLBP), "CAPEBFFFIJE")]
        [HarmonyPostfix]
        public static IEnumerator RemoveRankChangeSound_Postfix(IEnumerator incoming)
        {
            if (NoRanking.hideRankInGameResult.Value)
            {
                /*UnityEngine.Debug.Log("---------------------------- Stacktrace: \n" +
                    new System.Diagnostics.StackTrace());*/
                yield break;
            }
            else
            {
                yield return incoming;
            }
        }

    }
}
